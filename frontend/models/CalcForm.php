<?php

namespace frontend\models;

use yii\base\Model;

/**
 * Class CalcForm
 * @package frontend\models
 */
class CalcForm extends Model
{
    /** @var string */
    public $city;

    /** @var string */
    public $name;

    /** @var string */
    public $date;

    /** @var string */
    public $param1;

    /** @var string */
    public $param2;

    /** @var string */
    public $param3;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city', 'name', 'date'], 'required'],
            [['city', 'name', 'param1', 'param2', 'param3'], 'string', 'max' => 255],
            [['date'], 'date', 'format' => 'php:Y-m-d'],
        ];
    }
}