<?php

namespace frontend\models;

/**
 * Модель объекта ответа
 * @package frontend\models
 */
class Resp
{
    /** @var int */
    public $price;

    /** @var string */
    public $info;

    /** @var string */
    public $error;

}