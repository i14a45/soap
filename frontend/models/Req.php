<?php

namespace frontend\models;


use yii\base\BaseObject;

/**
 * Модель объекта запроса
 * @package frontend\models
 */
class Req extends BaseObject
{
    /** @var string */
    public $city;

    /** @var string */
    public $name;

    /** @var string */
    public $date;

    /** @var string */
    public $param1;

    /** @var string */
    public $param2;

    /** @var string */
    public $param3;
}