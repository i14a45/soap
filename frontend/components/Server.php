<?php

namespace frontend\components;

use Yii;
use yii\base\BaseObject;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;

/**
 * Class Server
 * @package frontend\components
 */
class Server extends BaseObject
{
    /** @var string */
    public $serviceClass;

    /** @var string */
    public $serverUrl;

    /** @var Service */
    protected $service;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if ($this->serviceClass === null) {
            throw new InvalidArgumentException('serviceClass must be set');
        }

        if ($this->serverUrl === null) {
            throw new InvalidArgumentException('serverUrl must be set');
        }

        try {
            $this->service = Yii::createObject($this->serviceClass);
        } catch (InvalidConfigException $e) {
            Yii::error($e->getMessage(), __CLASS__);
            exit(1);
        }
    }

    /**
     * @return false|string
     */
    public function run()
    {
        $server = new \SoapServer(null, [
            'uri' => $this->serverUrl,
        ]);

        try {
            $server->setObject($this->service);

            ob_start();
            $server->handle();
            $result = ob_get_contents();
            ob_end_clean();

            return $result;

        } catch (\Exception $e) {
            Yii::error($e->getMessage(), __CLASS__);
            $server->fault($e->getCode(), $e->getMessage());
            exit(1);
        }
    }

}