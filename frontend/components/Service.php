<?php

namespace frontend\components;

use Yii;
use frontend\models\Req;
use frontend\models\Resp;
use yii\base\BaseObject;

/**
 * Class Service
 * @package frontend\components
 */
class Service extends BaseObject
{
    /**
     * @param Req $req
     * @return Resp
     * @throws \yii\base\Exception
     */
    public function calculate($req)
    {
        $resp = new Resp();
        if ($req->date < date('Y-m-d')) {
            $resp->error = 'Date error';
        } else {
            $resp->price = mt_rand(1000, 100000);
            $resp->info = Yii::$app->security->generateRandomString();
        }
        return  $resp;
    }
}