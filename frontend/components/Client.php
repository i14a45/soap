<?php

namespace frontend\components;


use frontend\models\Req;
use yii\base\BaseObject;
use yii\base\InvalidArgumentException;

/**
 * Class Client
 * @package frontend\components
 */
class Client extends BaseObject
{
    /** @var string */
    public $serverUrl;

    /** @var string */
    public $apiKey;

    public function init()
    {
        parent::init();

        if ($this->serverUrl === null) {
            throw new InvalidArgumentException('wsdlUrl must be set');
        }
    }

    /**
     * @param Req $req
     * @return mixed
     */
    public function request($req)
    {
        $client = new \SoapClient(null, [
            'uri' => $this->serverUrl,
            'location' => $this->serverUrl,
            'soap_version' => 'SOAP_1_1',
            'stream_context' => stream_context_create([
                'http' => [
                    'header' => 'Authorization: Bearer ' . $this->apiKey,
                ],
            ]),
        ]);

        return $client->calculate($req);
    }
}