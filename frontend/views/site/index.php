<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\CalcForm */

$this->title = 'Form';
?>
<div class="site-index">
    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin([
                'id' => 'calc-form',
                'enableAjaxValidation' => true,
            ]); ?>

            <?= $form->field($model, 'city')->textInput(); ?>

            <?= $form->field($model, 'name')->textInput(); ?>

            <?= $form->field($model, 'date')->widget(\kartik\date\DatePicker::class, [
                'readonly' => true,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]); ?>

            <?= $form->field($model, 'param1')->textInput(); ?>

            <?= $form->field($model, 'param2')->textInput(); ?>

            <?= $form->field($model, 'param3')->textInput(); ?>


            <div class="form-group">
                <?= Html::submitButton('Рассчитать', ['class' => 'btn btn-primary', 'name' => 'calc-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <div id="result"></div>
        </div>
    </div>
</div>

<?php
$url = Url::to(['calculate']);
$this->registerJs(
<<<JS
$('#calc-form').on('beforeSubmit', function (e) {
    $.post("$url", $(this).serialize(), function(data) {
        if (data.data) {
            var error = data.data.error;
            var html = '';
            if (error) {
                html = '<div class="alert alert-danger">' + error + '</alert>'
            } else {
                html = '<div class="alert alert-success">Цена: ' + data.data.price + ' Инфо: ' + data.data.info + '</div>';
            }
            $("#result").html(html);
        }
    }, "json");
    return false;
});
JS
);