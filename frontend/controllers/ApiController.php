<?php

namespace frontend\controllers;

use Yii;
use frontend\components\Server;
use frontend\components\Service;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class ApiController
 * @package frontend\controllers
 */
class ApiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        Yii::$app->user->enableSession = false;
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::class,
        ];
        return $behaviors;
    }

    /**
     * @return false|string
     */
    public function actionIndex()
    {
        Yii::$app->response->format = Response::FORMAT_RAW;

        Yii::$app->response->headers->set('Content-Type', 'text/xml;charset=utf-8');

        $server = new Server([
            'serviceClass' => Service::class,
            'serverUrl' => Url::to([''], true),
        ]);

        return $server->run();
    }
}