<?php

namespace frontend\controllers;

use frontend\components\Client;
use frontend\models\CalcForm;
use frontend\models\Req;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\web\ErrorAction;


/**
 * Class SiteController
 * @package frontend\controllers
 */
class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ],
        ];
    }

    /**
     * @return array|string
     */
    public function actionIndex()
    {
        $model = new CalcForm();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    /**
     * @return array
     */
    public function actionCalculate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $result = [
            'data' => null,
        ];

        $model = new CalcForm();

        if ($model->load(Yii::$app->request->post())) {

            if (!$model->validate()) {
                // в нормальном сценарии сюда дойти не должно
                return [
                    'data' => [
                        'error' => 'Error processing request',
                    ],
                ];
            }

            $client = new Client([
                'serverUrl' => Url::to(['api/index'], true),
                'apiKey' => Yii::$app->params['api_key'],
            ]);

            $req = new Req($model->attributes);

            $result['data'] = $client->request($req);
        }

        return $result;
    }
}
