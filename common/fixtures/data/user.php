<?php

return [
    [
        'id' => 1,
        'username' => 'test',
        'auth_key' => 'test_api_key',
        'password_hash' => Yii::$app->security->generatePasswordHash('test'),
        'password_reset_token' => null,
        'email' => 'test@example.com',
        'status' => 10,
        'created_at' => 1500000000,
        'updated_at' => 1500000000,
    ],
];