```
./yii init
./yii migrate
./yii fixture User
```

#### calculate

##### Поля запроса:
Поле   | Тип    | Описание
:----- |:------:| :-----
city   | string | Город
name   | string | Имя
date   | string | Дата в формате yyyy-mm-dd
param1 | string |
param2 | string |
param3 | string |

##### Поля ответа
Поле   | Тип    | Описание
:----- |:------:| :-----
price  | int    | Цена
info   | string | Информация
error  | string | Сообщение об ошибке